<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * User Controller
 *
 * @property \App\Model\Table\UserTable $User
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserController extends AppController
{
    public function login()
    {
        $this->autoRender = false;
        $data = $this->request->getData();
        $name = "";
        if (isset($data["name"])) {
            $name = $data["name"];
        }    
        $password = "";
        if (isset($data['password'])) {
            $password = $data['password'];
        }    
        if ($name!="" && $password!="") {
            $user_table = $this->getTableLocator()->get('user');
            $user = $user_table->find()->where(["NAME"=>$name])->first();
            if (isset($user)) {
                if ($user["PASSWORD"]==$password) {
                    $query = $user_table->find('all')->where(["NAME" => $name]);
                    $json_array = json_encode($query->toArray());
                    echo $json_array;
                }else {
                    echo "failed";
                }
            }else{
                echo "failed";
            }
            
        }else {
            echo "failed";
        }
    }

    public function register()
    {
        $this->autoRender = false;
        $data = $this->request->getData();
        $name = "";
        if (isset($data["name"])) {
            $name = $data["name"];
        }       
        $password = "";
        if (isset($data['password'])) {
            $password = $data['password'];
        }    
        if ($password!="" && $name !="") {
            $user_table = $this->getTableLocator()->get('user');
            $user_name = $user_table->find('all')->where(['NAME'=>$name])->toArray();
            if (count($user_name) > 0) {
                echo "failed";
            }else {
                $user = $user_table->newEmptyEntity();
                $send_data = array('NAME'=>$name, 'PASSWORD'=>$password);
                $user = $user_table->patchEntity($user, $send_data);
                if ($user_table->save($user)) {
                    echo "success";
                }else{
                    echo "failed";
                }
            }  
        }else {
            echo "failed";
        }
    }

}
