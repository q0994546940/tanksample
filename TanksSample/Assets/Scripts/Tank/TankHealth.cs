﻿using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    public float startingHealth = 100f;
    public Slider slider;
    public Image fillImage;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;
    public GameObject explosionPrefab;

    private AudioSource m_ExplosionAudio;
    private ParticleSystem m_ExplosionParticles;
    private float m_CurrentHealth;
    private bool m_Dead;


    private void Awake()
    {
        m_ExplosionParticles = Instantiate(explosionPrefab).GetComponent<ParticleSystem>();
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

        m_ExplosionParticles.gameObject.SetActive(false);
    }


    private void OnEnable()
    {
        m_CurrentHealth = startingHealth;
        m_Dead = false;

        SetHealthUi();
    }

    public void TakeDamage(float amount)
    {
        // Adjust the tank's current health, update the UI based on the new health and check whether or not the tank is dead.
        if (gameObject.GetComponent<TankManager>().m_photonView.isMine)
        {
            GameManager.m_takeDame++; // +1 ダメージを受ける回数
        }
        

        m_CurrentHealth -= amount;

        SetHealthUi();

        if (m_CurrentHealth <= 0 && !m_Dead)
        {
            OnDeath();
        }
    }


    private void SetHealthUi()
    {
        // Adjust the value and colour of the slider.
        slider.value = m_CurrentHealth;


        fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, m_CurrentHealth / startingHealth);
    }


    private void OnDeath()
    {
        // Play the effects for the death of the tank and deactivate it.
        m_Dead = true;

        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive(true);

        m_ExplosionParticles.Play();
        if (m_ExplosionAudio != null)
        {
            m_ExplosionAudio.Play();
        }

        gameObject.SetActive(false);
    }
}