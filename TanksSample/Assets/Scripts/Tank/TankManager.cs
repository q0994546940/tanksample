﻿using System;
using Unity.VisualScripting;
using UnityEngine;

[Serializable]
public class TankManager:MonoBehaviour
{
    public PhotonView m_photonView;

    public Color m_player1Color = Color.blue;
    public Color m_player2Color = Color.red;

    public TankMovement m_Movement;       
    public TankShooting m_Shooting;
    public GameObject m_CanvasGameObject;

    private void Start()
    {
        SetUp();
    }

    private void SetUp()
    {
        if (m_photonView.isMine)
        {
            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.color = m_player1Color;
            }
        }
        else
        {
            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.color = m_player2Color;
            }
        }
    }

    public void DisableControl()
    {
        m_Movement.enabled = false;
        m_Shooting.enabled = false;

        m_CanvasGameObject.SetActive(false);
    }


    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Shooting.enabled = true;

        m_CanvasGameObject.SetActive(true);
    }
}
