using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class UserManager : MonoBehaviour
{
    [HideInInspector] public static int m_userID = -1;
    [HideInInspector] public static string m_userName = "";
}
