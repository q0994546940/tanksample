using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public GameObject m_roomPanelContent;
    public GameObject m_userIconPrefab;
    public GameObject m_startButton;
    public static bool m_roomJoined;
    private int m_nowNumberPlayer;
    private int m_oldNumberPlayer;
    // Start is called before the first frame update
    void Start()
    {
        m_roomJoined = false;   
    }

    // Update is called once per frame
    void Update()
    {
        //ルームに参加したら、人数を把握し、プレイヤーアイコンを作る
        if (m_roomJoined)
        {
            //ルームにいるプレイヤー人数がかわるときだけの処理
            m_nowNumberPlayer = PhotonNetwork.playerList.Length;
            if (m_nowNumberPlayer != m_oldNumberPlayer)
            {
                m_oldNumberPlayer = m_nowNumberPlayer;
                //Contentの中身を削除
                foreach (Transform child in m_roomPanelContent.transform)
                {
                    Destroy(child.gameObject);
                }
                //Contentの中身を再作成
                foreach (var player in PhotonNetwork.playerList)
                {
                    GameObject m_userIcon = Instantiate(m_userIconPrefab, Vector3.zero, Quaternion.identity);
                    m_userIcon.GetComponentInChildren<Text>().text = player.name;
                    m_userIcon.transform.SetParent(m_roomPanelContent.transform);
                }
            }
            if (m_nowNumberPlayer >= 2 && PhotonNetwork.isMasterClient)
            {
                m_startButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                m_startButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void OnClickStartButton()
    {
        PhotonNetwork.LoadLevel("BattleScene");
    }
}
