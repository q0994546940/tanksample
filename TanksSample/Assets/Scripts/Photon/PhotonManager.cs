using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonManager : Photon.MonoBehaviour
{
    public GameObject m_LoginedPanel;
    public Text m_userNameText;
    public GameObject m_joinButtonPrefab;
    public static bool m_logined;
    public GameObject m_roomListContent;


    public GameObject m_RoomPanel;
    public GameObject m_PlayerIcon;
    public GameObject m_RoomPanelContent;

    public void OnConnectedToPhoton()
    {
        Debug.Log("OnConnectedToPhoton");
    }
    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDisconnectedFromPhoton");
    }
    public void OnConnectionFail()
    {
        Debug.Log("OnConnectionFail");
    }
    public void OnFailedToConnectToPhoton(object parameters)
    {
        Debug.Log("OnFailedToConnectToPhoton");
    }
    public void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");

    }
    public void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
    }
    public void OnReceivedRoomListUpdate()
    {
        Debug.Log("OnReceivedRoomListUpdate");

        //スクロールビューの中身を削除
        GameObject content = GameObject.Find("RoomListContent");
        if (content != null)
        {
            foreach (Transform child in content.transform)
            {
                Destroy(child.gameObject);
            }
        }

        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        Debug.Log(string.Format("room count is {0}", rooms.Length));
        Debug.Log(rooms);
        foreach (RoomInfo infor in rooms)
        {
            GameObject m_joinButton = Instantiate(m_joinButtonPrefab, Vector3.zero, Quaternion.identity);
            m_joinButton.GetComponentsInChildren<Text>()[0].text = infor.name;
            m_joinButton.GetComponentsInChildren<Text>()[1].text = infor.playerCount.ToString() + "/2";
            m_joinButton.transform.SetParent(m_roomListContent.transform);
            if (infor.playerCount >= 2)
            {
                m_joinButton.GetComponent<Button>().interactable = false;
            }
            else
            {
                m_joinButton.GetComponent<Button>().interactable = true;
            }

        }
    }
    public void OnLeftLobby()
    {
        Debug.Log("OnLeftLobby");
    }
    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
        Debug.Log(string.Format("Name:{0}", PhotonNetwork.room.name));

    }
    public void OnPhotonCreateRoomFailed()
    {
        Debug.Log("OnPhotonCreateRoomFailed");
    }



    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        Debug.Log(string.Format("Name:{0}", PhotonNetwork.room.name));
        m_LoginedPanel.SetActive(false);
        m_RoomPanel.SetActive(true);
        RoomManager.m_roomJoined = true;
    }




    public void OnPhotonJoinRoomFailed(object[] cause)
    {
        Debug.Log("OnPhotonJoinRoomFailed");
    }
    public void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed");
    }
    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
    }
    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected");
    }
    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerDisconnected");
    }
    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched");
    }
    private void Start()
    {
        m_logined = false;
        m_LoginedPanel.SetActive(false);
        m_RoomPanel.SetActive(false);
    }
    private void Update()
    {
        if (m_logined)
        {
            m_logined = false;

            m_LoginedPanel.SetActive(true);
            m_userNameText.text = "Player: " + UserManager.m_userName;

            PhotonNetwork.ConnectUsingSettings("1.0");

            PhotonNetwork.sendRate = 60;
            PhotonNetwork.sendRateOnSerialize = 60;

            PhotonNetwork.automaticallySyncScene = true;

            PhotonNetwork.player.NickName = UserManager.m_userName;
        }
    }

    public void OnClickCreateRoomButton()
    {
        PhotonNetwork.CreateRoom(PhotonNetwork.player.NickName);
    }

    public void OnClickGameStart()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }

    public void OnClickLeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        m_RoomPanel.SetActive(false);
        m_LoginedPanel.SetActive(true);
    }
}
