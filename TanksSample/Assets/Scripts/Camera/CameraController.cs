using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private GameObject m_myTank;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetMyTank();
        if (m_myTank != null)
        {
            gameObject.transform.position = m_myTank.transform.position;
        }
    }

    private void SetMyTank()
    {
        if (GameManager.m_tanks != null)
        {
            if (GameManager.m_tanks.Length >= 2)
            {
                foreach (var tank in GameManager.m_tanks)
                {
                    if (tank.GetComponent<TankManager>().m_photonView.isMine)
                    {
                        m_myTank = tank;
                    }
                }
            }
        }
    }
}
