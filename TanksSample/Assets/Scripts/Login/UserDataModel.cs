using MiniJSON;
using System.Collections;
using System.Collections.Generic;

public class UserDataModel 
{
    public static List<UserData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<UserData>();

        IList jsonList = (IList)Json.Deserialize(sStrJson); //�f�R�[�h

        foreach (IDictionary jsonOne in jsonList)
        {
            var tmp = new UserData();
            if (jsonOne.Contains("ID"))
            {
                float.TryParse(jsonOne["ID"].ToString(), out float idValue);
                tmp.ID = (int)idValue;
            }
            if (jsonOne.Contains("NAME"))
            {
                tmp.NAME = (string)jsonOne["NAME"];
            }
            ret.Add(tmp);
        }

        return ret;
    }
}
