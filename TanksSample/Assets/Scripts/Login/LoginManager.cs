using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System;

public class LoginManager : MonoBehaviour
{
    //ログイン
    [SerializeField] private GameObject m_panelLogin;
    [SerializeField] private Text m_nameInputLogin = default;
    [SerializeField] private Text m_passwordInputLogin = default;
    [SerializeField] private Text m_errorTextLogin = default;
    //新規登録
    [SerializeField] private GameObject m_panelRegister;
    [SerializeField] private Text m_nameInputRegister = default;
    [SerializeField] private Text m_passwordInputRegister = default;
    [SerializeField] private Text m_errorTextRegister = default;

    private List<UserData> _userDataList;

    // Start is called before the first frame update
    void Start()
    {
        m_panelLogin.SetActive(true);
        m_panelRegister.SetActive(false);

        //既にログインした状態
        if (UserManager.m_userID >= 0 && UserManager.m_userName != "")
        {
            m_panelLogin.SetActive(false);
            m_panelRegister.SetActive(false);
            PhotonManager.m_logined = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }


    //Login処理<
    public void OnClickRegisterButtonLogin()
    {
        m_panelLogin.SetActive(false);
        m_panelRegister.SetActive(true);
    }
    public void OnClickLoginButtonLogin()
    {
        string url = "http://localhost/tank_project/User/login";
        string name = m_nameInputLogin.text;
        string password = m_passwordInputLogin.text;
        StartCoroutine(Login(
                url, name, password,
                WebRequestSuccessLogin,
                WebRequestFailedLogin
            ));
        
    }

    private IEnumerator Login(string url, string name, string password, Action<string> cbkSuccess = null, Action cbkFaild = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("password", password);
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();

        if (www.error != null)
        {
            Debug.Log(www.error);
            if (cbkFaild != null)
            {
                cbkFaild();
            }
        }
        else if (www.isDone) 
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (cbkSuccess != null)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
    private void WebRequestFailedLogin()
    {
        Debug.Log("ログイン失敗");
    }
    private void WebRequestSuccessLogin(string reponse)
    {
        

        if (reponse == "failed")
        {
            Debug.Log("ログイン失敗");
            m_errorTextLogin.text = "入力内容が間違っている、ログイン失敗！";
        }
        else
        {
            _userDataList = UserDataModel.DeserializeFromJson(reponse);
            SetUserDataList();
        }
    }
    private void SetUserDataList()
    {
        if (_userDataList != null)
        {
            foreach (UserData memberOne in _userDataList)
            {
                UserManager.m_userID = memberOne.ID;
                UserManager.m_userName = memberOne.NAME;
            }
            Debug.Log("ID:" + UserManager.m_userID + "   Name:" + UserManager.m_userName);
            m_panelLogin.SetActive(false);
            m_panelRegister.SetActive(false);
            PhotonManager.m_logined = true;
        }
    }
    //Login処理>
    //Register処理<
    public void OnClickLoginButtonRegister()
    {
        m_panelLogin.SetActive(true);
        m_panelRegister.SetActive(false);
    }
    public void OnClickRegisterButtonRegister()
    {
        string url = "http://localhost/tank_project/User/register";
        string name = m_nameInputRegister.text;
        string password = m_passwordInputRegister.text;
        StartCoroutine(Register(
                url, name, password,
                WebRequestSuccessRegister,
                WebRequestFailedRegister
            ));
    }

    private IEnumerator Register(string url, string name,  string password, Action<string> cbkSuccess = null, Action cbkFaild = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("password", password);
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            Debug.Log(www.error);
            if (cbkFaild != null)
            {
                cbkFaild();
            }
        }
        else if (www.isDone)
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (cbkSuccess != null)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
    private void WebRequestFailedRegister()
    {
        Debug.Log("登録失敗");
    }
    private void WebRequestSuccessRegister(string reponse)
    {
        if (reponse == "failed")
        {
            Debug.Log("登録失敗");
            m_errorTextRegister.text = "ユーザー名既存、登録失敗！";
        }
        else if (reponse == "success")
        {
            m_panelLogin.SetActive(true);
            m_panelRegister.SetActive(false);
        }
    }
}
