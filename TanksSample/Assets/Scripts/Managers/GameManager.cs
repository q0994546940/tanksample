using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [HideInInspector] public static GameObject[] m_tanks;
    [HideInInspector] public static  bool m_tankCreated;
    private GameObject m_winTank;

    public float m_StartDelay = 3f;
    public float m_EndDelay = 3f;
    public Text m_MessageText;

    private WaitForSeconds m_StartWait;
    private WaitForSeconds m_EndWait;

    //_Complete-GameScene用の変数
    [HideInInspector] public static int m_giveDame = 0;
    [HideInInspector] public static int m_takeDame = 0;
    public GameObject m_leaveRoomButton;

    private void Start()
    {
        if (PhotonNetwork.isMasterClient)
        {
            GameObject m_tank = PhotonNetwork.Instantiate("Tank", new Vector3(-3, 0, 30), Quaternion.Euler(0, -180, 0), 0);
        }
        else
        {
            GameObject m_tank = PhotonNetwork.Instantiate("Tank", new Vector3(13, 0, -5), Quaternion.Euler(0, 0, 0), 0);
        }


        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);
        m_tankCreated = false;   
        m_leaveRoomButton.SetActive(false);

    }

    private void Update()
    {
        if (FindTank() >= 2 && !m_tankCreated)
        {
            m_tankCreated = true;
            GetAllTanks();
            StartCoroutine(GameLoop());
        }
    }

    private int FindTank()
    {
        m_tanks = GameObject.FindGameObjectsWithTag("Player");
        return m_tanks.Length;
    }

    private void GetAllTanks()
    {
        m_tanks = GameObject.FindGameObjectsWithTag("Player");
    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(Starting());

        yield return StartCoroutine(Playing());

        yield return StartCoroutine(Ending());

        //ゲーム終わったら、PUNのルームを解散
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        m_leaveRoomButton.SetActive(true);
    }


    private IEnumerator Starting()
    {
        DisableTankControl();
        m_MessageText.text = "GAME START ";

        yield return m_StartWait;
    }


    private IEnumerator Playing()
    {
        EnableTankControl();

        m_MessageText.text = string.Empty;

        while (!OneTankLeft())
        {
            yield return null;
        }
    }


    private IEnumerator Ending()
    {
        DisableTankControl();

        foreach (var tank in m_tanks)
        {
            if (tank.activeSelf)
            {
                m_winTank = tank;
            }
        }

        string message = EndMessage();
        m_MessageText.text = message;

        yield return m_EndWait;
    }


    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        foreach (var tank in m_tanks)
        {
            if (tank.activeSelf)
            {
                numTanksLeft++;
            }
        }

        return numTanksLeft <= 1;
    }


    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_winTank != null)
        {
            if (m_winTank.GetComponent<TankManager>().m_photonView.isMine)
            {
                message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.blue) + ">YOU WIN \n\n 当たった弾：" + m_giveDame + "\n 当たられた弾：" + m_takeDame + " </color>"; ;
            }
            else
            {
                message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.red) + ">YOU LOST \n\n 当たった弾：" + m_giveDame + "\n 当たられた弾：" + m_takeDame + " </color>"; ;
            }
        }
        return message;
    }


    private void EnableTankControl()
    {
        foreach (var tank in m_tanks)
        {
            tank.GetComponent<TankManager>().EnableControl();
        }
    }


    private void DisableTankControl()
    {
        foreach (var tank in m_tanks)
        {
            tank.GetComponent<TankManager>().DisableControl();
        }
    }

    public void OnClickLeaveRoomButton()
    {
        PhotonNetwork.LoadLevel("LoginScene");
    }
}
